import {View, Image, Text} from '@tarojs/components'
import Taro, {Component} from '@tarojs/taro'

import './index.scss'

export default class EmptyView extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    static defaultProps = {
        emptyTips: '暂无数据',
        isNetError: false,
        isShowEmptyView: false,
        onReloadAction: null,
    }


    _reloadData = () => {
        this.props.onReloadAction()
    }

    render() {
        const {emptyTips, isNetError, isShowEmptyView} = this.props
        const {} = this.state
        return (
            <View>
                {isShowEmptyView && <View className="common-list-emptyContainer">
                    {isNetError ? (
                        <Image
                            src={require('../../assets/images/ic_no_network.png')}
                            style="height:200px;width:200px;"
                            mode="aspectFit"
                        />
                    ) : (
                        <Image
                            src={require('../../assets/images/ic_no_data.png')}
                            style="height:200px;width:200px;"
                            mode="aspectFit"
                        />
                    )}
                    <Text className='common-list-empty_tip'>{emptyTips}</Text>

                </View>}
            </View>
        )
    }
}

