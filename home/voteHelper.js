import TQPrism from '@tq-prism/prism'
import {Api} from '../../utils/Api'
import {md5} from "../../utils/md5";

//投票列表
export function getVoteList(data) {
  let path = '/project/findRankProject'

  return new Promise((resolve, reject) => {
    Api.get({
      path: path,
      data: data
    })
      .then(res => {
        console.log('getVoteList', res)
        resolve(res)
      }, error => {
        TQPrism.showToast({
          title: error,
          icon: 'none'
        })
        reject(error)
      })
      .catch(err => {
        console.log('getVoteList', 'err', err)
        reject('请求失败，请稍后重试')
      })
  })
}

//倒计时
export function getTimeDowm(data) {
  let path = '/user/vote/countdown'

  return new Promise((resolve, reject) => {
    Api.get({
      path: path,
      data: data
    }).then(res => {
      console.log('getTimeDowm', res)
      resolve(res)
    }, error => {
      TQPrism.showToast({
        title: error,
        icon: 'none'
      })
      reject(error)
    })
      .catch(err => {
        console.log('getTimeDowm', 'err', err)
        reject('请求失败')
      })
  })
}

//获取大类/小类/酒店列表
export function getCategory(data) {
  let path = '/category/findByLevel'

  return new Promise((resolve, reject) => {
    Api.get({
      path: path,
      data: data
    })
      .then(res => {
        console.log('getCategory', res)
        resolve(res)
      }, error => {
        TQPrism.showToast({
          title: error,
          icon: 'none'
        })
        reject(error)
      })
      .catch(err => {
        console.log('getReportIssueList', 'err', err)
        reject('请求出错，请稍后重试')
      })
  })
}

//投票
export function vote(data) {
  let path = '/user/vote/userVote'
  return new Promise((resolve, reject) => {
    Api.post({
      header: {
        'content-type': 'application/json'
      },
      path: path,
      data: data
    })
      .then(res => {
        console.log('vote:', res)
        resolve(res)
      }, error => {
        reject(error)
      })
      .catch(err => {
        console.log('vote:', 'err', err)
        reject('请求出错，请稍后重试')
      })
  })
}


//投票
export function vote2(data) {
  // ac: Config.authCode,
  //   ak: Config.tqPrismAppKey,
  //   projectId,
  //   oi: openId,
  //   system,
  //   platform,
  //   version,
  //   iv,
  //   ed: encryptedData,
  //   time: dayjs().valueOf()
  // data.platform = 'ios'
  // data.system = 'iOS 11.2.6'
  // data.time = 1621992787732

  const path = '/user/vote/vote'
  const {time, platform, system} = data;
  console.log(data)
  let currentVersion = md5(platform + system + time)
  console.log('currentVersion', currentVersion)
  currentVersion = md5(currentVersion + currentVersion.substring(0, 5))
  console.log('currentVersion2', currentVersion)
  currentVersion = currentVersion.substring(5, currentVersion.length - 5)
  console.log('currentVersion3', currentVersion)
  const random = Math.floor(Math.random() * 17)
  console.log('random', random)
  currentVersion = currentVersion.substring(random, currentVersion.length)
  console.log('currentVersion4', currentVersion)
  data.version = currentVersion
  return new Promise((resolve, reject) => {
    Api.post({
      header: {
        'content-type': 'application/json'
      },
      path: path,
      data: data
    })
      .then(res => {
        console.log('vote:', res)
        resolve(res)
      }, error => {
        reject(error)
      })
      .catch(err => {
        console.log('vote:', 'err', err)
        reject('请求出错，请稍后重试')
      })
  })
}

//获取数量
export function getVoteNumber(data) {
  let path = '/user/vote/totalcount'

  return new Promise((resolve, reject) => {
    Api.get({
      path: path,
      data: data
    })
      .then(res => {
        console.log('getVoteNumber:', res)
        resolve(res)
      }, error => {
        TQPrism.showToast({
          title: error,
          icon: 'none'
        })
        reject(error)
      })
      .catch(err => {
        console.log('getVoteNumber:', 'err', err)
        reject('请求出错，请稍后重试')
      })
  })
}




