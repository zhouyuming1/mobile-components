import TQPrism from '@tq-prism/prism'
import {Button, Image, Text, View} from '@tq-prism/components'
import "./index.scss";
import {AtModal, AtModalContent, AtTabs, AtTabsPane} from "@tq-prism/prism-ui"
import CountDownView from '../../components/countDownView'
import CommonList from "../../components/commonList"
import Tabs from "../../components/tabs"
import banner from '../../assets/images/icon_home_banner_bg.png'
import first from '../../assets/images/first.png'
import second from '../../assets/images/second.png'
import third from '../../assets/images/third.png'
import _isEmpty from 'lodash/isEmpty'
import closeImg from "../../assets/images/icon_common_close.png"
import dayjs from 'dayjs'
import Config from '../../config'
import {getCategory, getTimeDowm, getVoteList, getVoteNumber, vote2} from './voteHelper'
import {getErrorMsg} from "../../utils/StringUtil";
import detail from "../detail";

export default class Index extends TQPrism.Component {


  constructor(props) {
    super(props);

    this.config = {
      navigationBarTitleText: '首页'
    };
    this.state = {
      index1: 0,
      index2: 0,
      bigCategory: 0,//大类id
      smallCategory: 0,//小类id
      bigCategorys: [],//大类对象
      smallCategorys: [],//小类对象
      dataList: [],
      countdown: 0,
      days: 0,
      hours: 0,
      minute: 0,
      seconds: 0,
      tabList: [],
      appid: 'wxa4dfad69a65cbd77',
      personNum: 0,//参与人数
      voteNum: 0,//投票人数
      errorMsg: '',
      errorModal: false,
      currentTab: 0,
    }
  }

  tabClick(value) {
    this.setState({
      bigCategory: value
    })
  }

  componentDidShow() {
    //this.tickNumber()
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.interval2);
  }

  tick() {
    // console.log('定时器-----')
    this.calTime(this.state.countdown)
  }

  calTime(countdown) {
    // console.log('定时器')
    countdown--;
    if (countdown < 0) {
      clearInterval(this.interval);
      return
    }
    let d = parseInt(countdown / 60 / 60 / 24)

    let dayLeftS = countdown - d * 24 * 60 * 60

    let h = parseInt(dayLeftS / 60 / 60)

    let hourLeftS = dayLeftS - h * 60 * 60

    let m = parseInt(hourLeftS / 60)

    let s = countdown % 60


    this.setState({
      countdown: countdown,
      days: d,
      hours: h,
      minute: m,
      seconds: s,
    })
  }

  tickNumber() {
    // console.log("投票刷新--------")
    this.getNumber()
  }

  componentDidMount() {
    this.getBigCategory()
    // this.getSmallCategory()
    this.getTime()
    this.tickNumber()
    // TQPrism.eventCenter.on('homeRefresh', () => {
    //     this.tickNumber()
    // })
  }

  componentWillUnmount() {
    //TQPrism.eventCenter.off('homeRefresh')
  }

  getTime() {
    getTimeDowm({
      'appId': this.state.appid
    }).then(res => {
      // console.log(res)
      //测试暂停倒计时
      // res = 1
      this.calTime(res)
      this.interval = setInterval(() => this.tick(), 1000);
    })
  }

  //获取投票数量
  getNumber() {
    getVoteNumber({
      'appId': this.state.appid
    }).then(res => {
      console.log(res)
      this.setState({
        personNum: res.personNum,
        voteNum: res.voteNum
      })
      if (!this.interval2) {
        this.interval2 = setInterval(() => this.tickNumber(), 1000 * 60 * 5);
      }
    })
  }

  //获取大类
  getBigCategory() {
    getCategory({
      level: 1
    }).then(dataResult => {
      var names = new Array()
      var tabs = new Array()
      dataResult.map((item) => {
        const obj = {
          id: item.id,
          categoryName: item.categoryName,
          title: item.categoryName,
        }
        names.push(obj)
        tabs.push({'title':item.categoryName})
      })
      this.setState({
        bigCategorys: names,
        tabList: tabs,
        bigCategory: dataResult[0].id
      })
      console.log("大类id" + dataResult[0].id)
      this.getSmallCategory()
    })
  }

  //获取小类
  getSmallCategory() {
    getCategory({
      level: 2
    }).then(dataResult => {
      const names = new Array()
      dataResult.map((item) => {
        const obj = {
          id: item.id,
          categoryName: item.categoryName
        }
        names.push(obj)
      })
      this.setState(() => ({
        smallCategorys: names,
        smallCategory: dataResult[0].id
      }), () => {
        this.refresh()
      })
      console.log("小类id" + dataResult[0].id)
    })
  }

  getListData = (data) => {
    const {bigCategory, smallCategory} = this.state
    console.log("获取小类id-----" + smallCategory)
    let param = {
      projectSchemeId: bigCategory,
      projectPrizeId: smallCategory,
      pageSize: data.pageSize,
      pageNo: data.page
    }
    getVoteList(param).then(res => {
      const {rows} = res
      if (rows) {
        data.commonList.handleData(this, "dataList", rows)
      }
    }).catch(err => {
      console.log('getListData2:', err)
      data.commonList.handleData(this, "dataList")
    })

  }

  changeBigCategory(index) {
    const id = this.state.bigCategorys[index].id
    this.setState(() => ({
      bigCategory: id,
      index1: index,
    }), () => {
      this.refresh()
    })
    console.log('大类别-----' + id)

  }

  changeSmallCategory(item, index) {
    this.setState(() => ({
      smallCategory: item.id,
      index2: index,
    }), () => {
      this.refresh()
    })
    console.log('小类别-----' + item.id)

  }

  refresh() {
    if (this.CommonList) {
      console.log("调用子组件方法----------")
      this.CommonList.noSwipeRefresh() //调用子组件的dream方法
    }
  }

  getUser = (item, pos, e) => {
    // e.stopPropagation();
    // e.nativeEvent.stopImmediatePropagation();
    console.log('getUser', e)
    console.log('pos', pos)
    const {encryptedData, iv} = e.detail
    this.voteAction(item.id, pos, encryptedData, iv)
  }


  //投票
  async voteAction(projectId, pos, encryptedData, iv) {
    TQPrism.showLoading({
      title: "请稍后"
    })
    try {
      const {detail: {openId, avatarUrl, nickName}} = await TQPrism.getUserInfo({encryptedData, iv});
      const {version, platform, system} = await TQPrism.getSystemInfo();
      let param = {
        ac: Config.authCode,
        ak: Config.tqPrismAppKey,
        projectId,
        oi: openId,
        system,
        platform,
        version,
        iv,
        ed: encryptedData,
        time: dayjs().valueOf(),
        avatarUrl,
        nickName
      }
      vote2(param).then(res => {
        TQPrism.hideLoading()
        const data = {'status': true, 'date': dayjs().format('YYYY-MM-DD')}
        TQPrism.setStorageSync(projectId + '', JSON.stringify(data))
        console.log('date', JSON.stringify(data))
        const {dataList} = this.state
        dataList[pos].ticket++

        this.setState({
          dataList,
          voteNum: this.state.voteNum + 1
        })

        if (!_isEmpty(res) && res === '当前尚未关注法制日报公众号，请先关注') {
          this.setState({
            errorMsg: res,
            errorModal: true,
          })
        }
      }).catch(err => {
        TQPrism.hideLoading()
        if (!_isEmpty(err) && err === '当前尚未关注法制日报公众号，请先关注') {
          this.setState({
            errorMsg: err,
            errorModal: true,
          })
        } else {
          TQPrism.showToast({
            title: getErrorMsg(err),
            icon: 'none'
          })
        }
        console.log('voteAction:', err)
      })
    } catch (e) {
      TQPrism.hideLoading();
      TQPrism.showToast({
        title: getErrorMsg(e),
        icon: 'none'
      });
      console.log('voteAction:', e)
    }


  }

  onCellClick(item) {
    TQPrism.navigateTo({
      url: `/pages/detail/index?projectId=${item.id}&projectSchemeId=${this.state.bigCategory}&projectPrizeId=${this.state.smallCategory}&title1=${this.state.bigCategorys[this.state.index1].categoryName}&title2=${this.state.smallCategorys[this.state.index2].categoryName}&title3=${item.projectName}&title4=${item.organizationName}&source=home`
    })
    console.log('tile', this.state.bigCategorys[this.state.index1].categoryName, this.state.smallCategorys[this.state.index2].categoryName, item.projectName)
  }

  applyFollowOnClick() {
    this.setState({
      errorModal: false
    })

    TQPrism.navigateTo({
      url: `/pages/follow/index`
    })
  }

  onClickCloseError() {
    this.setState({
      errorModal: false
    })
  }

  handleClick (value) {
    this.setState({
      currentTab: value
    })
    this.changeBigCategory(value);
  }

  render() {
    const {dataList, tabList, smallCategorys, personNum, voteNum} = this.state
    const headerView = <View className='list-container'>
      <Image className='banner' src={banner}/>
      <View className='banner-container'>
        <Text className='banner-title'>2021政法</Text>
        <Text className='banner-title'>智能化建设创新案例投票</Text>
      </View>
      {/*<Button onClick={() => {*/}
      {/*  TQPrism.navigateTo({*/}
      {/*    url: `/pages/apply/index`*/}
      {/*  })*/}
      {/*}}>报名入口</Button>*/}
      <View className='cardView'>
        <View className='number-container'>
          <View className='number-view'>
            <Text className='number-count'>{personNum}</Text>
            <Text className='number-label'>参与人数</Text>
          </View>
          <View className='line-view'/>
          <View className='number-view'>
            <Text className='number-count'>{voteNum}</Text>
            <Text className='number-label'>累计投票</Text>
          </View>
        </View>
        <Text className='down-label'>投票结束倒计时:</Text>
        <CountDownView d={this.state.days} h={this.state.hours} m={this.state.minute} s={this.state.seconds}/>
      </View>
      <View className='tabView'>
        <AtTabs
            current={this.state.currentTab}
            scroll={true}
            tabList={tabList}
            onClick={this.handleClick.bind(this)}
            swipeable={false}
        >
        </AtTabs>
      </View>
      <View className='small-container'>
        {
          smallCategorys.map((item, index) => {
            return <Text
                className={item.id == this.state.smallCategory ? 'smallCategory' : 'smallCategory-normal'}
                onClick={this.changeSmallCategory.bind(this, item, index)} key={index}>
              {item.categoryName}
            </Text>
          })
        }
      </View>
    </View>

    return (
        <View className='container'>
          <CommonList onConcreteLoadData={this.getListData} ref={(ref) => {
            this.CommonList = ref
          }} renderHeader={headerView} enableHomeRefresh={true}>
            {
              dataList.map((item, index) => {
                    let data = TQPrism.getStorageSync(item.id + '')
                    let isTP = false
                    if (!_isEmpty(data)) {
                      const date1 = dayjs(dayjs().format('YYYY-MM-DD'))
                      const date2 = dayjs(JSON.parse(data)['date'])
                      if (date1.diff(date2, 'day') > 0) {
                        isTP = false
                      } else {
                        isTP = JSON.parse(data)['status']
                      }
                    }
                    return <View className='item-contain'>
                      <View className='content-container'>
                        {index === 0 ? <Image className='rank-img' src={first}/> : ''}
                        {index === 1 ? <Image className='rank-img' src={second}/> : ''}
                        {index === 2 ? <Image className='rank-img' src={third}/> : ''}
                        {index > 2 ? <Text className='rank-text'>{index + 1}</Text> : ''}
                        <View className='vote-container-content'
                              onClick={this.onCellClick.bind(this, item)}>
                          <Text className='vote-name'>{item.projectName}</Text>
                          {_isEmpty(item.organizationName) ? '' :
                              <Text className='complay-name'>{item.organizationName}</Text>}
                        </View>
                        <View className='vote-container'>
                          <Text className='vote-number'>{item.ticket}</Text>
                          <Button className={isTP ? 'voted-btn' : 'vote-btn'} open-type="getUserInfo"
                                  onGetUserInfo={this.getUser.bind(this, item, index)}>
                            {isTP ? '已投票' : '投票'}
                          </Button>
                        </View>
                      </View>
                      <View className='item-divider'/>
                    </View>
                  }
              )}
          </CommonList>
          <AtModal isOpened={this.state.errorModal}>
            <AtModalContent>
              <View className="container-content-atmodal">
                <View className="container-content-atmodal-top-img">
                  <Image src={closeImg} className="container-content-atmodal-img"
                         onClick={this.onClickCloseError.bind(this)}/>
                </View>
                <Text className="container-content-atmodal-content">{this.state.errorMsg}</Text>
                <Button className="container-content-atmodal-btn"
                        onClick={this.applyFollowOnClick.bind(this)}>去关注</Button>
              </View>
            </AtModalContent>
          </AtModal>
        </View>
    );
  }
}
