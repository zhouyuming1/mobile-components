import {View} from '@tarojs/components'
import Taro, {Component} from '@tarojs/taro'
import './index.scss'


export default class LoadMore extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        loadMoreState: 'loadMoreComplete',
        onLoadMoreFailAction: null
    }
    _bindLoadMoreFail = () => {
        this.props.onLoadMoreFailAction()
    }

    render() {
        const {loadMoreState} = this.props
        return (
            <View>
                {loadMoreState !== 'loadMoreComplete' &&< View className="common-list-loadmore-container">
                    {loadMoreState === 'loading'&&<View className="common-list-loading-progress"/>}
                {loadMoreState === 'loading' ? (
                    <View className="common-list-loadmore-tips">正在加载...</View>
                    ) : loadMoreState === 'loadMoreEnd' ? (
                    <View className="common-list-loadmore-tips-no">没有更多数据</View>
                    ) : (
                    loadMoreState === 'loadMoreFail' && (
                    <View
                    className="common-list-loadmore-fail-tips"
                    onClick={this._bindLoadMoreFail}
                    >
                    加载失败点击重试
                    </View>
                    )
                    )}
                    </View>
                    }
            </View>
        )
    }
}

