import { View, ScrollView } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'
import EmptyView from '../emptyView/index'
import LoadMore from '../loadMore/index'
import './index.scss'

const pageSize = 20

export default class CommonList extends Component {
    static defaultProps = {
        emptyTips: '暂无数据',
        errorNetTips: '当前网络不稳定,请稍后重试',
        enableRefresh: true,
        enableLoadMore: true,
        onConcreteLoadData: null,
        listHeight: null,
        enableHomeRefresh: false,
    }

    static externalClasses = ['i-class']

    constructor(props) {
        super(props);
        this.state = {
            loadMoreState: 'loadMoreComplete',
            isShowEmptyView: false,
            isNetError: false,
            //下拉刷新
            scrollHeight: 0,
            startY: 0,
            tips: '下拉刷新',
            isRefreshing: false,
        }
        this.page = 1
        this.oldPage = 1
    }

    //计算ScrollView的高度
    calculateScrollViewHeight() {
        let selectorQuery = null
        if (process.env.TARO_ENV === 'h5') {
            selectorQuery = Taro.createSelectorQuery().in(this)
        } else {
            selectorQuery = Taro.createSelectorQuery().in(this.$scope)
        }
        selectorQuery.select('#list-root')
            .boundingClientRect(rect => {
                const res = Taro.getSystemInfoSync()
                this.setState({
                    scrollViewHeight: res.windowHeight - rect.top
                })
            })
            .exec()

    }

    componentDidMount() {

        this.calculateScrollViewHeight()

        this._loadData()
    }


    componentWillUnmount() {

    }

    handleData(handler, dataKey, dataList) {
        if (this.page === 1) {
            this._stopRefresh()
            if (!dataList) {
                this.page = this.oldPage

                this.setState({
                    isShowEmptyView: handler.state[dataKey].length < 1,
                    isNetError: true,
                    loadMoreState: 'loadMoreComplete'
                })
            } else {
                if (dataKey && dataList && dataList.length > 0) {
                    this.setState({
                        isShowEmptyView: false,
                        isNetError: false,
                        loadMoreState:
                            dataList.length < pageSize ? 'loadMoreEnd' : 'loadMoreComplete'
                    })
                } else {
                    this.page = this.oldPage
                    this.setState({
                        isShowEmptyView: true,
                        isNetError: false,
                        loadMoreState: 'loadMoreComplete'
                    })
                }
            }
        } else {
            if (!dataList) {
                this.setState({
                    isShowEmptyView: false,
                    loadMoreState: 'loadMoreFail'
                })
                this.page--
            } else {
                this.setState({
                    isShowEmptyView: false,
                    loadMoreState:
                        dataList.length < pageSize ? 'loadMoreEnd' : 'loadMoreComplete'
                })
            }
        }

        if (handler && dataKey && dataList) {
            handler.setState({
                [dataKey]: this.page === 1 ? dataList : handler.state[dataKey].concat(dataList)
            })
        }
        if (this.page > 1 && !dataList && dataList.length === 0) {
            this.page--
        }
    }

    swipeRefresh = () => {
        if (this.props.enableRefresh) {
            this.page = 1
            this._loadData()
        }

        if (this.props.enableHomeRefresh) {
            //通知首页刷新数据
            TQPrism.eventCenter.trigger("homeRefresh", '')
        }
    }
    //刷新，页面不下拉
    noSwipeRefresh = () => {
        if (this.props.enableRefresh) {
            this.page = 1

            this.props.onConcreteLoadData({
                page: this.page,
                pageSize,
                commonList: this
            })
        }
    }
    _loadMoreData = () => {
        if (
            this.state.loadMoreState !== 'loading' &&
            this.state.loadMoreState !== 'loadMoreEnd' &&
            this.props.enableLoadMore
        ) {
            this.setState({
                loadMoreState: 'loading'
            })
            this.page++
            this._loadData()
            this.oldPage = this.page
        }
    }
    _loadData = () => {
        if (this.page === 1) {
            this._startRefresh()
        }

        this.props.onConcreteLoadData({
            page: this.page,
            pageSize,
            commonList: this
        })
    }
    _touchStart = e => {
        if (!this._canRefresh()) {
            return
        }
        this.setState({
            startY: e.touches[0].clientY
        })
    }
    _touchEnd = e => {
        if (!this._canRefresh()) {
            return
        }

        if (this.state.scrollHeight >= 50) {
            this._startRefresh()
            this.swipeRefresh()
        } else {
            this._stopRefresh()
        }
    }
    _touchMove = e => {
        if (!this._canRefresh()) {
            return
        }
        let moveY = e.touches[0].clientY

        let pullHeight = moveY - this.state.startY

        if (pullHeight > 0) {
            this.setState({
                scrollHeight: pullHeight >= 80 ? 80 : pullHeight
            })
        }
    }

    _canRefresh = () => {
        return !this.state.isRefreshing && this.props.enableRefresh
    }
    _stopRefresh = () => {
        this.setState({
            isRefreshing: false,
            scrollHeight: 0
        })
    }
    _startRefresh = () => {
        this.setState({
            isRefreshing: true,
            scrollHeight: 50
        })
    }


    render() {
        const {
            emptyTips,
            errorNetTips,
            enableRefresh,
            enableLoadMore,
            listHeight,
            enableHomeRefresh
        } = this.props
        const {
            scrollHeight,
            loadMoreState,
            isShowEmptyView,
            isNetError,
            scrollViewHeight
        } = this.state
        const resolveListHeight = listHeight != null ? listHeight : scrollViewHeight
        return (
            <ScrollView
                id='list-root'
                className='common-list-container'
                scrollY
                onScrollToLower={this._loadMoreData}
                onTouchEnd={this._touchEnd}
                onTouchMove={this._touchMove}
                onTouchStart={this._touchStart}
                style={`height:${resolveListHeight}px`}
            >
                <View
                    className='common-list-refresh-container'
                    style={`margin-top:${scrollHeight - 50}px`}
                >
                    <View className='common-list-refresh-progress' />
                </View>
                {this.props.renderHeader}
                {this.props.children}
                {this.props.renderFooter}
                {enableLoadMore && <LoadMore
                    loadMoreState={loadMoreState}
                    onLoadMoreFailAction={this._loadMoreData}
                />}
                <EmptyView
                    onReloadAction={this.swipeRefresh}
                    isShowEmptyView={isShowEmptyView}
                    isNetError={isNetError}
                    emptyTips={isNetError ? errorNetTips : emptyTips}
                />
            </ScrollView>

        )
    }
}

